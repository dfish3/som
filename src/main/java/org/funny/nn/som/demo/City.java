package org.funny.nn.som.demo;

/**
 * @author: LinLW
 */
public class City {
    public City(String city, double x, double y) {
        this.city = city;
        this.x = x;
        this.y = y;
    }

    public String getCity() {
        return city;

    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    private String city;
    private double x;
    private double y;
}