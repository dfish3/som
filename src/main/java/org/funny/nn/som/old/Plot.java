package org.funny.nn.som.old;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * 绘图
 * @author LinLW
 */
public class Plot {
    public static void plotNetwork(List<City> cities, List<org.funny.nn.som.old.Point> network, String fileName){
        //画一个256 像素的PNG

        BufferedImage bi = new BufferedImage(256, 256, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = bi.createGraphics();
        for(org.funny.nn.som.old.Point dot:network){
            drawDot(dot, Color.GREEN, graphics);
        }

        for(City city:cities){
            drawDot(city, Color.RED, graphics);
        }
        graphics.dispose();
        File f=new File(fileName);
        File folder=f.getParentFile();
        if(!folder.exists()){
            folder.mkdirs();
        }
        try(OutputStream os= new FileOutputStream(fileName)){
            ImageIO.write(bi,"PNG",os);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private static void drawDot(org.funny.nn.som.old.Point dot, Color color, Graphics2D graphics) {
        int x=(int)(dot.getX()*224)+16; //边框16像素不画内容。只画中间224像素
        int y=240-(int)(dot.getY()*224);
        graphics.setColor(color);
        graphics.drawLine(x,y,x,y);
    }
}
