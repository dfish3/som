package org.funny.nn.som.old;

/**
 * 用于存储位置信息的 类
 * @author: LinLW
 */
public class Point {
    public Point(){}
    public Point(double x,double y){
        this.x=x;
        this.y=y;
    }
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
    private double x;
    private double y;
}