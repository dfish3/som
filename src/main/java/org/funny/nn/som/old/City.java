package org.funny.nn.som.old;

/**
 * 用于存储城市信息的 类
 * @author: LinLW
 */
public class City extends Point{
    public City(String city, double x, double y) {
        super(x,y);
        this.city=city;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    private String city;

}