package org.funny.nn.som.old;

import java.util.List;

/**
 * 计算距离的类
 * @author: LinLW
 */
public class Distance {

    /**
     * 找出最近的一个节点
     * @param candidates 所有的候选项。这里是那层神经。
     * @param origin 目标城市
     * @return int 最近的那个候选项的序号。
     */
    public static int selectClosest(List<Point> candidates, Point origin){
        double dis=Float.MAX_VALUE;
        int find=-1;
        int i=0;
        for(Point p:candidates){
            double disCur= calcDistance(p,origin);
            if(disCur<dis){
                dis=disCur;
                find=i;
            }
            i++;
        }
        return find;
    }


    /**
     * 计算几何距离(勾股定理)
     * @param a City 城市a
     * @param b City 城市b
     * @return double 距离
     */
    public static double calcDistance(Point a, Point b){
        double xDist=a.getX()-b.getX();
        double yDist=a.getY()-b.getY();
        return Math.sqrt(xDist*xDist+yDist*yDist);
    }

    /**
     * 计算各个城市(有顺序的)形成的路径总长度
     * @param cities
     * @return double
     */
    public static double routeDistance(List<Point> cities){
        if(cities==null||cities.size()<1){
            return 0.0;
        }
        Point last=cities.get(cities.size()-1);
        double sum=0.0;
        for(Point p:cities){
            sum+= calcDistance(p,last);
            last=p;
        }
        return sum;
    }
}
